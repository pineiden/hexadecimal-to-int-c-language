#include <stdio.h>
#include <string.h>
#include <math.h>






#include "./hexchar2int.h"
#include "./checkheader.h"
#include "./hexchar2int.h"

#define BASE 16

int check_tail(char line[], unsigned int values[]){
  int counter=0;
  int i;
  size_t len =  strlen(line);
  int val;
  if (check_header(line)){
    for (i=2; i<len; i++){
        values[i-2] = hexchar2int(line[i])*pow(BASE, (len - i -1));
        if (values[i-2]>=0){
          counter++;
        }
    }
    if (counter == (len-2)){
      return 1;
    } else {
      return 0;
    }
  }  
  return 0;
};
