#include <stdio.h>






int hexchar2int(char hex){
  if ('0'<=hex && hex <= '9'){
     return hex - 48;
  } else if ('a'<hex && hex<'f'){
     return hex - 97;
  }else if('A'<hex && hex<'F'){
     return hex - 65;
  } else {
     return -1;
  }
};
