#include <stdio.h>






#include "./deps/structs.h"
#include "./deps/hex2int.h"

int main(){
  struct Int total = {.value = 0};
  char line[] = "0Xaa";
  unsigned result;
  result = hex2int(line, &total);
  printf("El valor decimal del hex %s es : %u\n", line, total.value);
  return 0;
}
