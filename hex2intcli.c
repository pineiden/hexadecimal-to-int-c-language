#include <stdio.h>
#include <stdlib.h>
#include <string.h>






#include "./deps/structs.h"
#include "./deps/hex2int.h"
#include "./deps/get_line.h"

#define MAXLINE 1024

void line2hex(char line[], char hex[]);

int main(){
  size_t len;
  unsigned result;
  char line[MAXLINE];

  struct Int total = {.value = 0};
  while ((len = get_line(line, MAXLINE)) > 0){
    // 

    char hex[len-1];
    memset(hex, 0, sizeof hex);
    line2hex(line, hex);
    result = hex2int(hex, &total);
    if (result){
      printf("El valor decimal del hex %s es : %u\n", hex, total.value);
    }
    memset(line, 0, sizeof hex);
    memset(hex, 0, sizeof line);
    memset(&total, 0, sizeof total);
  }

  return 0;
}

void line2hex(char line[], char hex[]){
   size_t i;
  i = 0;

  while ((line[i] != '\0') && (line[i] != '\n')){
    hex[i] = line[i];  
    ++i;}
  hex[i]='\0';
};
