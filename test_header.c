#include <stdio.h>






#include  "./e2_3/deps/checkheader.h"

int main(){
    char line1[] = "0x213213";
    char line2[] = "0x12321312";
    char line3[] = "0a123213";
    char line4[] = "Qa123213";    
    printf("Line: %s, result %d\n", line1, check_header(line1));
    printf("Line: %s, result %d\n", line2, check_header(line2));
    printf("Line: %s, result %d\n", line3, check_header(line3));
    printf("Line: %s, result %d\n", line4, check_header(line4));
    return 0;
}
